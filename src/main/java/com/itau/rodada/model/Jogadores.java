package com.itau.rodada.model;

import java.util.List;

public class Jogadores {
	private List<Jogador> jogadores;
	
	public List<Jogador> getJogadores() {
		return jogadores;
	}

	public void setJogadores(List<Jogador> jogadores) {
		this.jogadores = jogadores;
	}

	public int returnPontosJogador(int posicao) {
		return jogadores.get(posicao).getPontos();
	}
	
	public void setJogador(Jogador jogador, int posicao) {
		jogadores.set(posicao, jogador);
	}
	
	public int getID(int posicao) {
		return jogadores.get(posicao).getIdJogador();
	}
}
