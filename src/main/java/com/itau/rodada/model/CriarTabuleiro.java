package com.itau.rodada.model;

public class CriarTabuleiro {
	private boolean statusTabuleiro;

	public boolean isStatusTabuleiro() {
		return statusTabuleiro;
	}

	public void setStatusTabuleiro(boolean statusTabuleiro) {
		this.statusTabuleiro = statusTabuleiro;
	}

}
