package com.itau.rodada.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


public class Rodada {
    private Jogadores jogadores;
    private Jogo jogo;
    private Placar placar;

    public void iniciarJogo(){
        criarJogo();
    }

    public void jogar(Jogada jogada){
    	
    	jogo = gerarJogada(jogada);
    	
        if(jogo.isEncerrado()){
            return;
        }

        if(jogo.isEncerrado() && jogo.isVitoria()){
            int vencedor = jogo.getJogadorAtivo();
            Jogador jogador = somarPontos(jogadores.getID(vencedor));
            jogadores.setJogador(jogador, vencedor);
        }
    }
    
    
    public Jogadores obterJogadores(){
        System.out.println("=== Obter Jogadores ===");
        
    	Jogadores jogadores = new Jogadores();
    	
    	try {
			RestTemplate restTemplate = new RestTemplate();
			jogadores = restTemplate.getForObject("http://10.162.109.1:8000/jogadores", Jogadores.class);
    	}catch (HttpClientErrorException e) {
    		System.out.println("Erro especifico" + e.getResponseBodyAsString());
    	}catch(Exception e){
    		System.out.println("Erro especifico" + e.getMessage());
		}
    	
		return jogadores;
    }

    public void criarJogo(){
    	try {
			RestTemplate restTemplate = new RestTemplate();
			CriarTabuleiro criarTabuleiro = restTemplate.getForObject("http://10.162.108.170:8081/tabuleiro", CriarTabuleiro.class);
			if (!criarTabuleiro.isStatusTabuleiro()){
				System.out.println("Erro ao criar o tabuleiro");
			}
    	}catch (HttpClientErrorException e) {
    		System.out.println("Erro especifico" + e.getResponseBodyAsString());
		
    	}catch(Exception e){
    		System.out.println("Erro especifico" + e.getMessage());
		}
    	
    }
    
    public Jogo gerarJogada(Jogada jogada) {
    	//chamar API passando a jogada e retorna o Jogo
    	Jogo jogo = new Jogo();
    	try {
			RestTemplate restTemplate = new RestTemplate();
			jogo = restTemplate.postForObject("http://10.162.108.170:8081/jogada",jogada ,Jogo.class);
			
    	}catch (HttpClientErrorException e) {
    		System.out.println("Erro especifico" + e.getResponseBodyAsString());
		
    	}catch(Exception e){
    		System.out.println("Erro especifico" + e.getMessage());
		}
    	
    	return jogo; 
    	
    }
    
    public Jogador somarPontos(int id) {
    	
    	Jogador jogador = new Jogador();
    	
    	try {
			RestTemplate restTemplate = new RestTemplate();
			jogador = restTemplate.postForObject("http://10.162.109.1:8000/pontos",id ,Jogador.class);
			
    	}catch (HttpClientErrorException e) {
    		System.out.println("Erro especifico" + e.getResponseBodyAsString());
		
    	}catch(Exception e){
    		System.out.println("Erro especifico" + e.getMessage());
		}
    	
    	return jogador;
    	
    }

    public Placar getPlacar(){
    	System.out.println("=== Jogadores ===");
        jogadores = obterJogadores();
        int[] pontuacao = {jogadores.returnPontosJogador(0), jogadores.returnPontosJogador(1)};
        System.out.println("===   Pontuacao  " + pontuacao[0] + " " + pontuacao[1]);  
        
        placar.pontuacao = pontuacao;
        placar.casas = retornaTabuleiro();
        placar.encerrado = jogo.isEncerrado();

        return placar;
    }
    
    public String[][] retornaTabuleiro(){
    	System.out.println("===   Entrei no Tabuleiro");
    	Tabuleiro tabuleiro = new Tabuleiro();
    	try {
			RestTemplate restTemplate = new RestTemplate();
			tabuleiro = restTemplate.getForObject("http://10.162.108.170:8081/casas", Tabuleiro.class);
    	}catch (HttpClientErrorException e) {
    		System.out.println("Erro especifico" + e.getResponseBodyAsString());
    	}catch(Exception e){
    		System.out.println("Erro especifico" + e.getMessage());
		}
    	
		return tabuleiro.getCasas();
    }

}