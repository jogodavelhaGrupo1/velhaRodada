package com.itau.rodada.model;

public class Jogada {
    public int x;
    public int y;
	
    @Override
	public String toString() {
		return "Jogada [x=" + x + ", y=" + y + "]";
	}
    
}
