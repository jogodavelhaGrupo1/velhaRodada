package com.itau.rodada.model;

public class Jogo {

    private int jogadorAtivo;
    private boolean encerrado;
    private boolean vitoria;
    
	public boolean isEncerrado() {
		return encerrado;
	}
	public void setEncerrado(boolean encerrado) {
		this.encerrado = encerrado;
	}
	public boolean isVitoria() {
		return vitoria;
	}
	public void setVitoria(boolean vitoria) {
		this.vitoria = vitoria;
	}
	public int getJogadorAtivo() {
		return jogadorAtivo;
	}
	public void setJogadorAtivo(int jogadorAtivo) {
		this.jogadorAtivo = jogadorAtivo;
	}
	
    
}
