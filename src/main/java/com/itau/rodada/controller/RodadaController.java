package com.itau.rodada.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.rodada.model.Jogada;
import com.itau.rodada.model.Jogador;
import com.itau.rodada.model.Placar;
import com.itau.rodada.model.Rodada;


@Controller
@CrossOrigin
public class RodadaController {
	
	Rodada rodada;
	Jogador jogador1;
	Jogador jogador2;
	

    @RequestMapping("/iniciar")
    public @ResponseBody
    Placar iniciarJogo() {
    	
    	System.out.println("=== Iniciar ==="); 
        rodada.iniciarJogo();

        return rodada.getPlacar();
    }
	
	@RequestMapping("/")
	public @ResponseBody
	Placar getPlacar() {
		
		System.out.println("=== Placar ==="); 
		rodada = new Rodada();
		return rodada.getPlacar();
	}

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public @ResponseBody
    Placar jogar(@RequestBody Jogada jogada) {
        
    	System.out.println("=== Jogada " + jogada.toString());
    	rodada.jogar(jogada);

        return rodada.getPlacar();
    }
	
}
